#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>

class Hash
{
    int BUCKET;    // No. of buckets

    // Pointer to an array containing buckets
    std::vector<int> *table;
public:
    Hash(int V);  // Constructor

    // inserts a key into hash table
    void insertItem(uint64_t x);

    // deletes a key from hash table
    void deleteItem(int key);

    // hash function to map values to key
    int hashFunction(uint64_t x) {
        return (x % 1000000);
    }

    void displayHash();
};

Hash::Hash(int b)
{
    this->BUCKET = b;
    table = new std::vector<int>[BUCKET];
}

void Hash::insertItem(uint64_t key)
{
    int index = hashFunction(key);
    std::cout<<index;
    table[index].push_back(key);
}

void Hash::deleteItem(int key)
{
  // get the hash index of key
  int index = hashFunction(key);

  // find the key in (index)th list
  std::vector<int> :: iterator i;
  for (i = table[index].begin();
           i != table[index].end(); i++) {
    if (*i == key)
      break;
  }

  // if key is found in hash table, remove it
  if (i != table[index].end())
    table[index].erase(i);
}

// function to display hash table
void Hash::displayHash() {
  for (int i = 0; i < BUCKET; i++) {
    std::cout << i;
    for (auto x : table[i])
      std::cout << " --> " << x;
    std::cout << std::endl;
  }
}

// Driver program
int main()
{
  // array that contains keys to be mapped
  std::string a[] = {"GD12FDD", "11", "27", "8", "8", "12"};
  std::vector<int> vals;
  std::string b = "";
  int n = sizeof(a)/sizeof(a[0]);

  // insert the keys into the hash table
  Hash h(n);   // 7 is count of buckets in
               // hash table

  std::string s;
  for (int i = 0; i < n; i++){
    s = "";
    for (int j = 0; j < a[i].length(); j++){
    s = s + std::to_string((int)a[i][j]);
    vals.push_back((int)a[i][j]);
  }

  std::cout<<s<<std::endl;
  uint64_t a;
  char* end;
  a = strtoull(s.c_str(), &end, 10);
    // std::cout<<vals[i]<<std::endl;
    h.insertItem(a);
  }
  for (int i=0; i<vals.size(); i++){
    // std::cout<<vals[i]<<std::endl;
  }

  // delete 12 from hash table
  h.deleteItem(0);

  // display the Hash table
  h.displayHash();

  return 0;
}
